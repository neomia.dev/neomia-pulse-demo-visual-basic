﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TestPulseForm
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TestPulseForm))
        Me.userRefTextBox = New System.Windows.Forms.TextBox()
        Me.loginButton = New System.Windows.Forms.Button()
        Me.resetBioButton = New System.Windows.Forms.Button()
        Me.passwordLabel = New System.Windows.Forms.Label()
        Me.usernameLabel = New System.Windows.Forms.Label()
        Me.titleLabel = New System.Windows.Forms.Label()
        Me.passwordTextBox = New System.Windows.Forms.TextBox()
        Me.addRefButton = New System.Windows.Forms.Button()
        Me.logoPictureBox = New System.Windows.Forms.PictureBox()
        Me.clearButton = New System.Windows.Forms.Button()
        Me.loginPanel = New System.Windows.Forms.Panel()
        Me.loginPanelLabel = New System.Windows.Forms.Label()
        Me.optionPanel = New System.Windows.Forms.Panel()
        Me.optionPanelLabel = New System.Windows.Forms.Label()
        Me.infoButton = New System.Windows.Forms.Button()
        Me.retrackButton = New System.Windows.Forms.Button()
        CType(Me.logoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.loginPanel.SuspendLayout()
        Me.optionPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'userRefTextBox
        '
        Me.userRefTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.userRefTextBox.Location = New System.Drawing.Point(166, 71)
        Me.userRefTextBox.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.userRefTextBox.Name = "userRefTextBox"
        Me.userRefTextBox.Size = New System.Drawing.Size(478, 35)
        Me.userRefTextBox.TabIndex = 0
        '
        'loginButton
        '
        Me.loginButton.BackColor = System.Drawing.Color.MidnightBlue
        Me.loginButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.loginButton.ForeColor = System.Drawing.Color.White
        Me.loginButton.Location = New System.Drawing.Point(305, 179)
        Me.loginButton.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.loginButton.Name = "loginButton"
        Me.loginButton.Size = New System.Drawing.Size(112, 60)
        Me.loginButton.TabIndex = 3
        Me.loginButton.Text = "Login"
        Me.loginButton.UseVisualStyleBackColor = False
        '
        'resetBioButton
        '
        Me.resetBioButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.resetBioButton.Location = New System.Drawing.Point(14, 66)
        Me.resetBioButton.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.resetBioButton.Name = "resetBioButton"
        Me.resetBioButton.Size = New System.Drawing.Size(136, 42)
        Me.resetBioButton.TabIndex = 3
        Me.resetBioButton.Text = "Reset Bio. Data"
        Me.resetBioButton.UseVisualStyleBackColor = True
        '
        'passwordLabel
        '
        Me.passwordLabel.AutoSize = True
        Me.passwordLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.passwordLabel.Location = New System.Drawing.Point(21, 138)
        Me.passwordLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.passwordLabel.Name = "passwordLabel"
        Me.passwordLabel.Size = New System.Drawing.Size(132, 29)
        Me.passwordLabel.TabIndex = 4
        Me.passwordLabel.Text = "Password :"
        '
        'usernameLabel
        '
        Me.usernameLabel.AutoSize = True
        Me.usernameLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.usernameLabel.Location = New System.Drawing.Point(21, 75)
        Me.usernameLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.usernameLabel.Name = "usernameLabel"
        Me.usernameLabel.Size = New System.Drawing.Size(136, 29)
        Me.usernameLabel.TabIndex = 5
        Me.usernameLabel.Text = "Username :"
        '
        'titleLabel
        '
        Me.titleLabel.AutoSize = True
        Me.titleLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.titleLabel.Location = New System.Drawing.Point(356, 48)
        Me.titleLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.titleLabel.Name = "titleLabel"
        Me.titleLabel.Size = New System.Drawing.Size(210, 33)
        Me.titleLabel.TabIndex = 6
        Me.titleLabel.Text = ".net Connexion"
        '
        'passwordTextBox
        '
        Me.passwordTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.passwordTextBox.Location = New System.Drawing.Point(166, 129)
        Me.passwordTextBox.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.passwordTextBox.Name = "passwordTextBox"
        Me.passwordTextBox.Size = New System.Drawing.Size(478, 35)
        Me.passwordTextBox.TabIndex = 1
        Me.passwordTextBox.UseSystemPasswordChar = True
        '
        'addRefButton
        '
        Me.addRefButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.addRefButton.Location = New System.Drawing.Point(183, 66)
        Me.addRefButton.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.addRefButton.Name = "addRefButton"
        Me.addRefButton.Size = New System.Drawing.Size(134, 42)
        Me.addRefButton.TabIndex = 8
        Me.addRefButton.Text = "Add as ref."
        Me.addRefButton.UseVisualStyleBackColor = True
        '
        'logoPictureBox
        '
        Me.logoPictureBox.BackgroundImage = CType(resources.GetObject("logoPictureBox.BackgroundImage"), System.Drawing.Image)
        Me.logoPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.logoPictureBox.Location = New System.Drawing.Point(164, 18)
        Me.logoPictureBox.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.logoPictureBox.Name = "logoPictureBox"
        Me.logoPictureBox.Size = New System.Drawing.Size(172, 69)
        Me.logoPictureBox.TabIndex = 9
        Me.logoPictureBox.TabStop = False
        '
        'clearButton
        '
        Me.clearButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.clearButton.Location = New System.Drawing.Point(482, 189)
        Me.clearButton.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.clearButton.Name = "clearButton"
        Me.clearButton.Size = New System.Drawing.Size(136, 42)
        Me.clearButton.TabIndex = 10
        Me.clearButton.Text = "Clear"
        Me.clearButton.UseVisualStyleBackColor = True
        '
        'loginPanel
        '
        Me.loginPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.loginPanel.Controls.Add(Me.loginPanelLabel)
        Me.loginPanel.Controls.Add(Me.usernameLabel)
        Me.loginPanel.Controls.Add(Me.userRefTextBox)
        Me.loginPanel.Controls.Add(Me.clearButton)
        Me.loginPanel.Controls.Add(Me.passwordLabel)
        Me.loginPanel.Controls.Add(Me.passwordTextBox)
        Me.loginPanel.Controls.Add(Me.loginButton)
        Me.loginPanel.Location = New System.Drawing.Point(18, 106)
        Me.loginPanel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.loginPanel.Name = "loginPanel"
        Me.loginPanel.Size = New System.Drawing.Size(670, 248)
        Me.loginPanel.TabIndex = 11
        '
        'loginPanelLabel
        '
        Me.loginPanelLabel.AutoSize = True
        Me.loginPanelLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.loginPanelLabel.Location = New System.Drawing.Point(22, 12)
        Me.loginPanelLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.loginPanelLabel.Name = "loginPanelLabel"
        Me.loginPanelLabel.Size = New System.Drawing.Size(106, 20)
        Me.loginPanelLabel.TabIndex = 6
        Me.loginPanelLabel.Text = "Login panel"
        '
        'optionPanel
        '
        Me.optionPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.optionPanel.Controls.Add(Me.retrackButton)
        Me.optionPanel.Controls.Add(Me.infoButton)
        Me.optionPanel.Controls.Add(Me.optionPanelLabel)
        Me.optionPanel.Controls.Add(Me.resetBioButton)
        Me.optionPanel.Controls.Add(Me.addRefButton)
        Me.optionPanel.Location = New System.Drawing.Point(18, 398)
        Me.optionPanel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.optionPanel.Name = "optionPanel"
        Me.optionPanel.Size = New System.Drawing.Size(670, 127)
        Me.optionPanel.TabIndex = 12
        '
        'optionPanelLabel
        '
        Me.optionPanelLabel.AutoSize = True
        Me.optionPanelLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optionPanelLabel.Location = New System.Drawing.Point(16, 12)
        Me.optionPanelLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.optionPanelLabel.Name = "optionPanelLabel"
        Me.optionPanelLabel.Size = New System.Drawing.Size(125, 20)
        Me.optionPanelLabel.TabIndex = 12
        Me.optionPanelLabel.Text = "Options panel"
        '
        'infoButton
        '
        Me.infoButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.infoButton.Location = New System.Drawing.Point(510, 66)
        Me.infoButton.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.infoButton.Name = "infoButton"
        Me.infoButton.Size = New System.Drawing.Size(134, 42)
        Me.infoButton.TabIndex = 13
        Me.infoButton.Text = "Info"
        Me.infoButton.UseVisualStyleBackColor = True
        '
        'retrackButton
        '
        Me.retrackButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.retrackButton.Location = New System.Drawing.Point(343, 66)
        Me.retrackButton.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.retrackButton.Name = "retrackButton"
        Me.retrackButton.Size = New System.Drawing.Size(134, 42)
        Me.retrackButton.TabIndex = 14
        Me.retrackButton.Text = "Retrack"
        Me.retrackButton.UseVisualStyleBackColor = True
        '
        'TestPulseForm
        '
        Me.AcceptButton = Me.loginButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(706, 546)
        Me.Controls.Add(Me.optionPanel)
        Me.Controls.Add(Me.loginPanel)
        Me.Controls.Add(Me.logoPictureBox)
        Me.Controls.Add(Me.titleLabel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.MinimizeBox = False
        Me.Name = "TestPulseForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pulse Test VB Form"
        CType(Me.logoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.loginPanel.ResumeLayout(False)
        Me.loginPanel.PerformLayout()
        Me.optionPanel.ResumeLayout(False)
        Me.optionPanel.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents userRefTextBox As TextBox
    Friend WithEvents loginButton As Button
    Friend WithEvents resetBioButton As Button
    Friend WithEvents passwordLabel As Label
    Friend WithEvents usernameLabel As Label
    Friend WithEvents titleLabel As Label
    Friend WithEvents passwordTextBox As TextBox
    Friend WithEvents addRefButton As Button
    Friend WithEvents logoPictureBox As PictureBox
    Friend WithEvents clearButton As Button
    Friend WithEvents loginPanel As Panel
    Friend WithEvents loginPanelLabel As Label
    Friend WithEvents optionPanel As Panel
    Friend WithEvents optionPanelLabel As Label
    Friend WithEvents infoButton As Button
    Friend WithEvents retrackButton As Button
End Class
