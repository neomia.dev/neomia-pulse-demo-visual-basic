﻿Imports neomia_pulse_agent_c_sharp
Imports neomia_pulse_agent_c_sharp.api
Imports neomia_pulse_agent_c_sharp.exception.base
Imports neomia_pulse_agent_c_sharp.model

Public Class TestPulseForm

    Private Const ApiUrl As String = "https://api.api-ai.systancia.com/pulse" 'Your API URL (please, change me)
    Private Const ApiKey As String = "WDX92pimeqZ3OmsFdWXU63fm3zhWzuKc" 'Your API key (please change me)

    Private lastPatternId As String = Nothing

    Class PulseInstanceNotGeneratedException
        Inherits System.Exception

    End Class


    Private Shared Sub InitPulseApi()
        PulseApi.Build(ApiUrl, ApiKey)
    End Sub

    Private Sub TestPulseForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.addRefButton.Enabled = False

        'The user is opening the windows. Pulse is necessary, then I build it.
        InitPulseApi()

        ' Enable Pulse to read new biometrics behavior data.

        Dim pulseRec As PulseRec = PulseRec.GetInstance()

        pulseRec.StartTracking()

    End Sub


    Private Sub TestPulseForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing

        'The user is closing the windows... Pulse is unnecessary, then I destroy it.
        ' Disallow Pulse to read new biometrics behavior data.

        Dim pulseRec As PulseRec = PulseRec.GetInstance()




        pulseRec.Reset()
    End Sub


    Private Sub LoginButton_Click(sender As Object, e As EventArgs) Handles loginButton.Click


        Dim password As String = passwordTextBox.Text
        Dim userRef As String = userRefTextBox.Text 'My custom data in Pulse task


        If String.IsNullOrWhiteSpace(userRef) Or userRef.Length <= 3 Then
            MsgBox("The username cannot be empty", MsgBoxStyle.OkOnly, "Invalid username error")
            Return
        End If


        If String.IsNullOrEmpty(password) Then
            MsgBox("The password cannot be empty", MsgBoxStyle.OkOnly, "Empty password error")
            Return
        End If

        Me.Enabled = False

        Me.lastPatternId = Nothing

        Me.addRefButton.Enabled = False

        Dim succesFunction = Function(authentication As Authentication, myUseful As String)
                                 PulseAuthenticationProcess(authentication, myUseful)
                             End Function


        Dim failedFunction = Function(pulseException As PulseException, myUseful As String)
                                 pulseException.Display()
                                 PulseAuthenticationProcess(Nothing, myUseful)
                             End Function


        Dim pulseApi As PulseApi = PulseApi.GetInstance()
        Dim pulseRec As PulseRec = PulseRec.GetInstance()

        Dim pattern = pulseRec.GetPattern()

        pulseApi.UserVerifyAsync(userRef, pattern, True, userRef, succesFunction, failedFunction)

    End Sub

    Private Sub AddRefButton_Click(sender As Object, e As EventArgs) Handles addRefButton.Click

        'Add last pattern as reference.

        If Me.lastPatternId Is Nothing Then
            Return
        End If

        Me.Enabled = False

        Dim succesFunction = Function(status As Boolean, myUseful As String)
                                 If status Then
                                     MsgBox("Pattern added for user : " + myUseful)
                                 Else
                                     MsgBox("Add pattern failed")
                                 End If
                                 Me.Enabled = True
                             End Function


        Dim failedFunction = Function(pulseException As PulseException, myUseful As String)
                                 pulseException.Display()
                                 Me.Enabled = True
                             End Function


        Dim userRef As String = userRefTextBox.Text 'My custom data in Pulse task

        Dim pulseApi As PulseApi = PulseApi.GetInstance()

        pulseApi.UserActionAsync(userRef, PulseApi.Action.LinkPatternId, Me.lastPatternId, userRef, succesFunction, failedFunction)

        Me.lastPatternId = Nothing

    End Sub


    Private Sub ResetButton_Click(sender As Object, e As EventArgs) Handles resetBioButton.Click

        'Reset the user biometrics profile.

        Me.Enabled = False


        Dim succesFunction = Function(status As Boolean, myUseful As String)
                                 If status Then
                                     MsgBox(myUseful + " biometrics profil has been reset")
                                 Else
                                     MsgBox("Reset failed")
                                 End If
                                 Me.Enabled = True
                             End Function


        Dim failedFunction = Function(pulseException As PulseException, myUseful As String)
                                 pulseException.Display()
                                 Me.Enabled = True
                             End Function


        Dim userRef As String = userRefTextBox.Text 'My custom data in Pulse task

        Dim pulseApi As PulseApi = PulseApi.GetInstance()

        pulseApi.UserActionAsync(userRef, PulseApi.Action.ResetBiometricProfile, Nothing, userRef, succesFunction, failedFunction)

    End Sub


    Private Sub ClearButton_Click(sender As Object, e As EventArgs) Handles clearButton.Click

        userRefTextBox.Clear()
        passwordTextBox.Clear()

        Dim pulseRec As PulseRec = PulseRec.GetInstance()

        pulseRec.ClearStack()
    End Sub

    Private Sub PulseAuthenticationProcess(authentication As Authentication, userRef As String)

        Me.Enabled = True

        If authentication Is Nothing Then

            MsgBox("An error has been encountered during the authentication process..." & Environment.NewLine & "Please try again !", MsgBoxStyle.OkOnly, "Authentication failed")

            'Authentication object is null, auth process failed, nothing to do.
            Return
        End If

        Dim action As Authentication.Action? = authentication.GetAction()

        If action = Authentication.Action.Enrolled Then
            MsgBox(userRef + " has been enrolled by Pulse")
            Return
        End If

        Dim rc As Authentication.RecommendedAction? = authentication.GetRecommendedAction()
        Dim score As Double? = authentication.GetFinalScore()

        If rc Is Nothing Or score Is Nothing Then
            MsgBox(userRef + " authentication failed")
            Return
        End If

        Dim rcMessage As String = ""

        Select Case rc
            Case Authentication.RecommendedAction.Auth
                rcMessage = "authenticated"
            Case Authentication.RecommendedAction.Challenge
                rcMessage = "challenged"
            Case Authentication.RecommendedAction.Block
                rcMessage = "block"
        End Select

        passwordTextBox.Clear()

        Dim pulseRec As PulseRec = PulseRec.GetInstance()

        pulseRec.ClearStack()

        Me.lastPatternId = authentication.GetPatternTestId()
        Me.addRefButton.Enabled = True

        MsgBox(userRef + " " + rcMessage + " with a score of " + Math.Round(CDbl(score * 100)).ToString() + "%")
    End Sub

    Private Sub InfoButton_Click(sender As Object, e As EventArgs) Handles infoButton.Click

        Dim pulseRec As PulseRec = PulseRec.GetInstance()

        Dim inputInfo As String() = PulseRec.InputControlsCompatibilityInfo()

        Dim message As String = ""

        message += "Automatic compatibility input :" + Environment.NewLine
        For Each item As String In inputInfo
            message += Environment.NewLine + item
        Next

        MsgBox(message, 0, "Pulse Info")

    End Sub

    Private Sub RetrackButton_Click(sender As Object, e As EventArgs) Handles retrackButton.Click

        Dim pulseRec As PulseRec = PulseRec.GetInstance()

        pulseRec.Reset()
        pulseRec.StartTracking()

    End Sub
End Class
